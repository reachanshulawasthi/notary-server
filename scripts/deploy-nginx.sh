#!/usr/bin/env bash

set -euo pipefail

cd "$(dirname $0)/.."

GLOBAL_TIMEOUT=5m

printf "\n### Deploying nginx ingress controller\n\n"

kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.0.0/deploy/static/provider/cloud/deploy.yaml


printf "\n### Waiting for nginx ingress controller to report ready\n"
kubectl wait --for=condition=Available deployments --all --namespace ingress-nginx  --timeout=${GLOBAL_TIMEOUT}