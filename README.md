# Notary Kubernetes

## Requirements

- Docker installed and running.


1.  Clone this repository and cd into it.

    ```
    ~ # git clone https://github.com/rio/notary-kubernetes.git
    ...snip...

    ~ # cd notary-kubernetes
    ```

2.  Run the `scripts/download-tools.sh` script to pull in all tools required.
    The script will create a `bin` folder at the root of the repo and then
    downloads and verifies them before making them executable. If you want the
    scripts to use these tools automatically you'll have to add the `bin` folder
    to your path as well. This change to your PATH will only be valid in this terminal.
    Or you could move them to a location already your path like `/usr/local/bin`.

    ```
    ~/notary-kubernetes # ./scripts/download-tools.sh
    ## Downloading binaries
    kubectl         DONE
    kustomize       DONE
    helm            DONE
    k3d             DONE
    notary          DONE

    ## Validating binaries
    kubectl: OK
    kustomize: OK
    helm: OK
    k3d: OK
    notary: OK

    Do not forget to add the /root/notary-kubernetes/bin directory to your path so other
    scripts can use these binaries. Run the following command in the root of
    this repository to enable the bin folder for this terminal.

        export PATH=$PATH:$PWD/bin

    ~/notary-kubernetes # export PATH=$PATH:$PWD/bin
    ```

3.  Run `scripts/preflight-check.sh` to determine if your system is ready for
    deployment. If everything checks out it should look like this and you can
    continue to step 4 to deploy everything.

    ```
    ~/notary-kubernetes # ./scripts/preflight-check.sh
    Looking for required binaries

    kubectl installed       ✓       (version: Client Version: v1.20.0                       path: /root/notary-kubernetes/bin/kubectl)
    kustomize installed     ✓       (version: {kustomize/v3.8.9  2020-12-29T15:49:08Z  }    path: /root/notary-kubernetes/bin/kustomize)
    helm installed          ✓       (version: v3.4.2+g23dd3af                               path: /root/notary-kubernetes/bin/helm)

    All required binaries found.

    Looking for required services

    docker:                 ✓       (version: 20.10.2)
    kubernetes:             ✓       (version: ServerVersion:v1.19.4+k3s1    context: k3d-k3s-default        user: admin@k3d-k3s-default)

    All checks passed.
    ```

4.  Deploy all services using the `scripts/deploy.sh` script. The commands
    executed by the script are idempotent. If anything goes wrong during installation
    like a timeout is reached because of a slow or disconnected internet connection
    you can just rerun the script.

    > **WARNING**: It is not recommended to run this script against any production
    > cluster. It will install a number of cluster wide resources like CRDs, Namespaces,
    > ClusterRoles and ClusterRoleBindings. Verify that you're not running this against
    > an unexpected cluster with unexpected privileges by running the `preflight-check.sh`
    > script again.

    ```
    ~/notary-kubernetes # ./scripts/deploy.sh
    # Deploying dependencies

    ## Timeout when deploying dependencies: 5m

    ### Deploying cert-manager

    customresourcedefinition.apiextensions.k8s.io/certificaterequests.cert-manager.io configured
    customresourcedefinition.apiextensions.k8s.io/certificates.cert-manager.io configured
    customresourcedefinition.apiextensions.k8s.io/challenges.acme.cert-manager.io configured
    customresourcedefinition.apiextensions.k8s.io/clusterissuers.cert-manager.io configured
    customresourcedefinition.apiextensions.k8s.io/issuers.cert-manager.io configured
    customresourcedefinition.apiextensions.k8s.io/orders.acme.cert-manager.io configured
    namespace/cert-manager created
    serviceaccount/cert-manager-cainjector created
    serviceaccount/cert-manager created
    serviceaccount/cert-manager-webhook created
    clusterrole.rbac.authorization.k8s.io/cert-manager-cainjector unchanged
    clusterrole.rbac.authorization.k8s.io/cert-manager-controller-issuers unchanged
    clusterrole.rbac.authorization.k8s.io/cert-manager-controller-clusterissuers unchanged
    clusterrole.rbac.authorization.k8s.io/cert-manager-controller-certificates unchanged
    clusterrole.rbac.authorization.k8s.io/cert-manager-controller-orders unchanged
    clusterrole.rbac.authorization.k8s.io/cert-manager-controller-challenges unchanged
    clusterrole.rbac.authorization.k8s.io/cert-manager-controller-ingress-shim unchanged
    clusterrole.rbac.authorization.k8s.io/cert-manager-view unchanged
    clusterrole.rbac.authorization.k8s.io/cert-manager-edit unchanged
    clusterrolebinding.rbac.authorization.k8s.io/cert-manager-cainjector unchanged
    clusterrolebinding.rbac.authorization.k8s.io/cert-manager-controller-issuers unchanged
    clusterrolebinding.rbac.authorization.k8s.io/cert-manager-controller-clusterissuers unchanged
    clusterrolebinding.rbac.authorization.k8s.io/cert-manager-controller-certificates unchanged
    clusterrolebinding.rbac.authorization.k8s.io/cert-manager-controller-orders unchanged
    clusterrolebinding.rbac.authorization.k8s.io/cert-manager-controller-challenges unchanged
    clusterrolebinding.rbac.authorization.k8s.io/cert-manager-controller-ingress-shim unchanged
    role.rbac.authorization.k8s.io/cert-manager-cainjector:leaderelection unchanged
    role.rbac.authorization.k8s.io/cert-manager:leaderelection unchanged
    role.rbac.authorization.k8s.io/cert-manager-webhook:dynamic-serving created
    rolebinding.rbac.authorization.k8s.io/cert-manager-cainjector:leaderelection unchanged
    rolebinding.rbac.authorization.k8s.io/cert-manager:leaderelection configured
    rolebinding.rbac.authorization.k8s.io/cert-manager-webhook:dynamic-serving created
    service/cert-manager created
    service/cert-manager-webhook created
    deployment.apps/cert-manager-cainjector created
    deployment.apps/cert-manager created
    deployment.apps/cert-manager-webhook created
    mutatingwebhookconfiguration.admissionregistration.k8s.io/cert-manager-webhook configured
    validatingwebhookconfiguration.admissionregistration.k8s.io/cert-manager-webhook configured

    ### Waiting for cert-manager to report ready
    deployment.apps/cert-manager condition met
    deployment.apps/cert-manager-cainjector condition met
    deployment.apps/cert-manager-webhook condition met

    ### Deploying nginx ingress controller

    namespace/ingress-nginx created
    serviceaccount/ingress-nginx created
    configmap/ingress-nginx-controller created
    clusterrole.rbac.authorization.k8s.io/ingress-nginx unchanged
    clusterrolebinding.rbac.authorization.k8s.io/ingress-nginx unchanged
    role.rbac.authorization.k8s.io/ingress-nginx created
    rolebinding.rbac.authorization.k8s.io/ingress-nginx created
    service/ingress-nginx-controller-admission created
    service/ingress-nginx-controller created
    deployment.apps/ingress-nginx-controller created
    ingressclass.networking.k8s.io/nginx unchanged
    validatingwebhookconfiguration.admissionregistration.k8s.io/ingress-nginx-admission configured
    serviceaccount/ingress-nginx-admission created
    clusterrole.rbac.authorization.k8s.io/ingress-nginx-admission unchanged
    clusterrolebinding.rbac.authorization.k8s.io/ingress-nginx-admission unchanged
    role.rbac.authorization.k8s.io/ingress-nginx-admission created
    rolebinding.rbac.authorization.k8s.io/ingress-nginx-admission created
    job.batch/ingress-nginx-admission-create created
    job.batch/ingress-nginx-admission-patch created

    ### Waiting for nginx ingress controller to report ready
    deployment.apps/ingress-nginx-controller condition met

    ### Deploying postgres

    NAME    NAMESPACE       REVISION        UPDATED                                 STATUS          CHART                   APP VERSION
    notary  notary          1               2021-10-24 22:25:44.7970212 +0530 IST   deployed        postgresql-10.2.4       11.10.0

    ## Deploying dependencies complete

    ## Deploying certificates

    secret/notary-ca created
    secret/notary-ca-cert created
    certificate.cert-manager.io/notaryserver-tls created
    certificate.cert-manager.io/notarysigner-tls created
    certificate.cert-manager.io/postgres-tls created
    issuer.cert-manager.io/notary-ca created

    ### Waiting for certificates to report ready
    certificate.cert-manager.io/notarysigner-tls condition met
    certificate.cert-manager.io/postgres-tls condition met
    certificate.cert-manager.io/registry-tls condition met
    certificate.cert-manager.io/notaryserver-tls condition met

    # Deploying Notary 

    ## Timeout when deploying Notary : 5m

    ### Deploying notary 

    Warning: resource namespaces/notary is missing the kubectl.kubernetes.io/last-applied-configuration annotation which is required by kubectl apply. kubectl apply should only be used on resources created declaratively by either kubectl create --save-config or kubectl apply. The missing annotation will be patched automatically.
    namespace/notary configured
    configmap/notaryserver-26cdfcb9c2 created
    configmap/notarysigner-dt77g798g7 created
    configmap/scripts-b8bhh25dh5 created
    secret/notarysigner-4g6k44c8c8 created
    service/notary created
    deployment.apps/notary created
    job.batch/migrate created
    ingress.networking.k8s.io/notary-ingress created

    ### Waiting for deployments to report ready

    deployment.apps/notary condition met

    ## Deploying notary and registry complete

    # Deployment complete
    ```

5.  Validate that we can reach the registry and that Notary is functioning as expected.
    We will run `scripts/verify.sh` which is loosly based on the [Docker Trust Content guide](https://docs.docker.com/engine/security/trust/)
    that Docker provides.

    > **WARNING**: This script will generate keys and change data in `~/.docker/trust`.
    > If you are not running this inside the sandbox container and have data in your
    > `~/.docker/trust` folder that you do not want to lose you should make a backup!

    It will:
    - Pull in an image.
    - Tag the image so we can push it to our own registry.
    - Generate a role with a private/public key pair on our machine for signing.
    - Add that key as a signer for our image repository. This will generate a new root key and repository key.
    - Sign our image with our private key. This will also trigger a push to the registry.
    - Verify if Docker fails a pull on an unsigned image and succeeds with a signed image.

    ```
    $ ./verify.sh 
    WARNING! Using --password via the CLI is insecure. Use --password-stdin.
    Login Succeeded
    # Exercising registry

    ## Pulling registry.gitlab.com/reachanshulawasthi/snyk-poc image

    Using default tag: latest
    latest: Pulling from reachanshulawasthi/snyk-poc
    Digest: sha256:eea80ed4c6cde097c45b8c078bc9394f71acc0e9d2f7ac1a44e13915b95f1ae3
    Status: Image is up to date for registry.gitlab.com/reachanshulawasthi/snyk-poc:latest
    registry.gitlab.com/reachanshulawasthi/snyk-poc:latest

    ## Tagging registry.gitlab.com/reachanshulawasthi/snyk-poc as registry.gitlab.com/reachanshulawasthi/snyk-poc:unsigned

    + docker tag registry.gitlab.com/reachanshulawasthi/snyk-poc registry.gitlab.com/reachanshulawasthi/snyk-poc:unsigned

    ## Pushing registry.gitlab.com/reachanshulawasthi/snyk-poc:unsigned image

    The push refers to repository [registry.gitlab.com/reachanshulawasthi/snyk-poc]
    ef130cb711ae: Layer already exists
    e802b269bb7f: Layer already exists
    1d30cb6475dd: Layer already exists
    fc36d2e5903b: Layer already exists
    347937b58a8e: Layer already exists
    4b9ebd9f2937: Layer already exists
    9a5d14f9f550: Layer already exists
    unsigned: digest: sha256:eea80ed4c6cde097c45b8c078bc9394f71acc0e9d2f7ac1a44e13915b95f1ae3 size: 1788

    # Registry functional

    # Exercising Notary

    ## Reusing laptop-a7f42q9l.pub public key.
    ## If this is undesireable then delete this file.

    ## Adding laptop-a7f42q9l.pub as signer for registry.gitlab.com/reachanshulawasthi/snyk-poc root passphrase 'root' and repository passphrase 'repo'

    Adding signer "laptop-a7f42q9l" to registry.gitlab.com/reachanshulawasthi/snyk-poc...
    Error: error contacting notary server: x509: certificate signed by unknown authority

    Failed to add signer to: registry.gitlab.com/reachanshulawasthi/snyk-poc
    ```

    <!-- Finally let's list the target signature that we just pushed to Notary. This
    should be similar to the output of the `docker trust inspect --pretty localhost/library/alpine`
    command that our `verify.sh` script ran at the end.

    ```
    ~/notary-kubernetes # notary -c config/notary-client.json list localhost/library/alpine
    NAME      DIGEST                                                              SIZE (BYTES)    ROLE
    ----      ------                                                              ------------    ----
    signed    074d3636ebda6dd446d0d00304c4454f468237fdacf08fb0eeac90bdbfa1bac7    528             targets/831d0e07ce16 -->
    ```
